" General vim Settings
set t_Co=256			"set vim to use 256 colors
"set term=xterm-256color "set the term to xterm-256--may break some terms
filetype indent on      "make use of language-specific indentations
set nocompatible		"use vim defaults
set noshowmode
set ls=2				"always show status line
set tabstop=4			"numbers of spaces of tab character
set softtabstop=4       "change the spaces of tabs while editing
set shiftwidth=4		"number of spaces to (auto)indent
set scrolloff=3			"keep 3 lines when scrolling
"set expandtab			"use spaces instead of tabs
set showcmd				"display incomplete commands
set hlsearch			"highlight searches
set incsearch			"do incremental searches
set ruler				"show the cursor position all the time
"set visualbell t_vb	"turn off error beep/flash
set nobackup			"do not keep a backup file
set ignorecase			"ignore case when searching
"set noignorecase		"don't ignore case when searching
set title				"show title in console title bar
set ttyfast				"smoother changes
"set ttyscroll=0		"turn off scrolling
set modeline			"last lines in document sets vim mode
set modelines=3			"number of lines checked for modelines
set shortmess=atI		"abbreviate messages
set nostartofline		"don't jump to the first character when paging
set wrap				"word wrap on
set linebreak           "keep words intact on the same line when breaking
set textwidth=0         "turn off hard wrapping
set colorcolumn=80      "show a line at 80 characters
"set spell              "automatic spell checking
"set nolist             "enable line break option
set whichwrap=b,s,h,l,<,>,[,]	"move freely between files
set autoindent			"always set autoindent on
set smartindent			"smart indent
set number              "add line numbers on the left
"set noautoindent		"turn off autoindent
set background=dark     "force the background color
set wildmenu            "visual autocomplete for command menu
set showmatch           "show matching [{()}]
set foldenable          "enable text folding
set foldlevelstart=10   "open most folds by default
set foldnestmax=10      "don't let folds get too crazy
set foldmethod=indent   "fold based on indent level
set cursorline          "highlight the current line
set showcmd             "show the last run command at the bottom
syntax enable			"syntax highlighting on
set bs=2                "backspace fix for some systems

" Allow for true colors
let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
set termguicolors

" Vundle Plugins and Settings
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'lesliev/vim-inform7'
Plugin 'flazz/vim-colorschemes'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'morhetz/gruvbox'
Plugin 'trusktr/seti.vim'
Plugin 'crusoexia/vim-dracula'
Plugin 'reedes/vim-thematic'
Plugin 'reedes/vim-pencil'
Plugin 'tyrannicaltoucan/vim-quantum'
Plugin 'soli/prolog-vim'
Plugin 'junegunn/vim-journal'
Plugin 'tpope/vim-surround'
Plugin 'scrooloose/syntastic'
Plugin 'altercation/vim-colors-solarized'
Plugin 'ajh17/spacegray.vim'
Plugin 'gsiano/vmux-clipboard'
Plugin 'w0rp/ale'
Plugin 'lervag/vimtex'
call vundle#end()            " required
filetype plugin indent on    " required

" Settings for Airline Statusbar
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_powerline_fonts = 1
let g:airline_section_x = '%{PencilMode()}'

" Settings for Ale Linter
let g:ale_completion_enabled = 1

" Enable for Solarized Theme
"colorscheme solarized
"set background=dark
"let g:airline_theme = 'base16_solarized'
"let g:solarized_termcolors=256
"let g:solarized_termtrans=1

" Enable for Gruvbox Theme
colorscheme gruvbox
let g:airline_theme = 'gruvbox'
let g:gruvbox_termcolors = '256'
let g:gruvbox_contrast_dark = 'hard'
let g:gruvbox_italic = 1
let g:gruvbox_italicize_comments = 1

" Enable for Quantum Theme
"colorscheme quantum
"let g:airline_theme = 'quantum'
"let g:quantum_black = 1
"let g:quantum_italics = 1

" Enable for Spacegray Theme
"colorscheme spacegray
"let g:spacegray_use_italics = 1
"let g:spacegray_low_contrast = 1
"let g:spacegray_underline_search = 1
"let g:airline_theme = 'base16_spacemacs'

" Enable for Dracula Theme
"colorscheme dracula
"let g:airline_theme = 'dracula'
"let g:dracula_italic = 1

" GUI Specific Settings
if has("gui_running")
    set guifont=Hack\ Regular:h14
    set guioptions-=r
    set guioptions-=m
    set guioptions-=T
    "set transparency=5
endif

" TMUX Settings
" the following allows the cursor to change in tmux mode
if exists('$TMUX')
    let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
    let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif

" Basic Color Schemes
"colorscheme elflord
"colorscheme wombat
"colorscheme xoria256
"colorscheme inkpot
"colorscheme gardener
"colorscheme simple-dark
"colorscheme Tomorrow-Night-Eighties
"colorscheme Tomorrow-Night-Bright
"colorscheme ironman
"colorscheme coffee
"colorscheme matrix
"colorscheme seti
